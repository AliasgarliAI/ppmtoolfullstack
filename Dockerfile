FROM openjdk:22-oracle
COPY build/libs/ppmtool-0.0.1-SNAPSHOT.jar /app/
WORKDIR /app/
ENTRYPOINT ["java" ]
CMD ["-jar", "/app/ppmtool-0.0.1-SNAPSHOT.jar"]
#"-Dspring.profiles.active=local"