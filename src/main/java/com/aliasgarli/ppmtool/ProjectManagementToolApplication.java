package com.aliasgarli.ppmtool;

import com.aliasgarli.ppmtool.dto.ProjectDto;
import com.aliasgarli.ppmtool.entity.Project;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.LocalDateTime;

@SpringBootApplication
public class ProjectManagementToolApplication{

	public static void main(String[] args) {
		SpringApplication.run(ProjectManagementToolApplication.class, args);
	}


}
