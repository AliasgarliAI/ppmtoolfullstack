package com.aliasgarli.ppmtool.config;

import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MapperConfig {

    @Bean(name = "projectMapper")
    public ModelMapper projectModelMapper(){
        return new ModelMapper();
    }

}
