package com.aliasgarli.ppmtool.constants;

public class ExceptionMessages {
    public static final String USER_NOT_FOUND = "There is not any user with the specified data";
    public static final String USERS_NOT_FOUND = "There are not existing any users";
    public static final String RESULT_IS_NOT_FOUND = "Result is not found";
    public static final String PROJECT_NOT_FOUND = "There are not existing projects";
}
