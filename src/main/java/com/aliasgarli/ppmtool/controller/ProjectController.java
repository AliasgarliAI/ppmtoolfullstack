package com.aliasgarli.ppmtool.controller;

import com.aliasgarli.ppmtool.dto.HttpResponse;
import com.aliasgarli.ppmtool.dto.ProjectDto;
import com.aliasgarli.ppmtool.exception.NotFoundException;
import com.aliasgarli.ppmtool.service.inter.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class ProjectController {

    private final ProjectService projectService;

    @GetMapping("/projects")
    @ResponseStatus(HttpStatus.OK)
    public HttpResponse<List<ProjectDto>> getProjects(){
        List<ProjectDto> projects = projectService.getAllProjects();

        return HttpResponse.success(projects,HttpStatus.OK,"");
    }

    @GetMapping("/projects/{projectId}")
    public HttpResponse<ProjectDto> getProjectById(@PathVariable Long projectId) throws NotFoundException {
        ProjectDto project = projectService.getProjectById(projectId);

        return HttpResponse.success(project,HttpStatus.OK,"");
    }
}
