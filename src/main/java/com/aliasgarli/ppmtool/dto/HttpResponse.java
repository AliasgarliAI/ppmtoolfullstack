package com.aliasgarli.ppmtool.dto;

import lombok.Builder;
import lombok.Data;
import org.springframework.http.HttpStatus;

@Data
@Builder
public class HttpResponse<T> {

    private Boolean success;
    private String message;
    private HttpStatus statusCode;
    private T data;

    private HttpResponse() {
    }

    public static <T> HttpResponse<T> empty() {
        return HttpResponse.<T>builder()
                .success(false)
                .data(null)
                .message("")
                .statusCode(HttpStatus.NO_CONTENT)
                .build();
    }

    public static <T> HttpResponse<T> success(T data, HttpStatus statusCode,String message) {
        return HttpResponse.<T>builder()
                .success(true)
                .data(data)
                .message(message)
                .statusCode(statusCode)
                .build();
    }

    public static <T> HttpResponse<T> error(String message, HttpStatus statusCode) {
        return HttpResponse.<T>builder()
                .success(false)
                .data(null)
                .message(message)
                .statusCode(statusCode)
                .build();
    }
}
