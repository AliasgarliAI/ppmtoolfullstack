package com.aliasgarli.ppmtool.dto;

import jakarta.persistence.Column;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class ProjectDto {

    private Long id;
    @NotBlank(message = "Project name is required")
    private String projectName;
    @NotBlank(message ="Project Identifier is required")
    @Size(min=4, max=5, message = "Please use 4 to 5 characters")
    @Column(updatable = false, unique = true)
    private String projectIdentifier;
    @NotBlank(message = "Project description is required")
    private String description;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private LocalDate start_date;
    @DateTimeFormat(pattern = "yyyy-mm-dd")
    private LocalDate end_date;
    @DateTimeFormat(pattern = "yyyy-mm-dd hh:mm")
    @Column(updatable = false)
    private LocalDateTime created_At;
    @DateTimeFormat(pattern = "yyyy-mm-dd hh:mm")
    private LocalDateTime updated_At;
}
