package com.aliasgarli.ppmtool.exception;

import com.aliasgarli.ppmtool.dto.HttpResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@Slf4j
@RestControllerAdvice
public class CustomExceptionHandler {


    @ExceptionHandler(NotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public HttpResponse<NotFoundException> notFoundException(NotFoundException ex){
        return HttpResponse.error(ex.getMessage(), HttpStatus.NOT_FOUND);
    }
}
