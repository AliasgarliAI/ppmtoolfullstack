package com.aliasgarli.ppmtool.mapper;

import com.aliasgarli.ppmtool.dto.ProjectDto;
import com.aliasgarli.ppmtool.entity.Project;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProjectMapper {

    ProjectDto projectToDto(Project project);

    Project dtoToProject(ProjectDto projectDto);

    List<ProjectDto> projectsToDtoList(List<Project> projects);

    List<Project> DtoListToProjects(List<ProjectDto> dtoList);

}
