package com.aliasgarli.ppmtool.repository;

import com.aliasgarli.ppmtool.entity.Project;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ProjectRepository extends JpaRepository<Project,Long> {

    @Query(value = "select u from project u where u.project_name like /%?1%/ ",nativeQuery = true)
    List<Project> findProjectByProjectName(String projectName);
    Optional<Project> findProjectByProjectIdentifier(String projectIdentifier);


}
