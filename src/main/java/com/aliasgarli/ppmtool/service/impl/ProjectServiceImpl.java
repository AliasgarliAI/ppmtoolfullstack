package com.aliasgarli.ppmtool.service.impl;

import com.aliasgarli.ppmtool.dto.HttpResponse;
import com.aliasgarli.ppmtool.dto.ProjectDto;
import com.aliasgarli.ppmtool.entity.Project;
import com.aliasgarli.ppmtool.exception.NotFoundException;
import com.aliasgarli.ppmtool.mapper.ProjectMapper;
import com.aliasgarli.ppmtool.repository.ProjectRepository;
import com.aliasgarli.ppmtool.service.inter.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

import static com.aliasgarli.ppmtool.constants.ExceptionMessages.*;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private ProjectMapper projectMapper;

    @Override
    public List<ProjectDto> getAllProjects() {
        return projectMapper.projectsToDtoList(projectRepository.findAll());
    }

    @Override
    public ProjectDto getProjectById(Long projectId) throws NotFoundException {

        Project project = projectRepository.findById(projectId)
                .orElseThrow(() -> new NotFoundException(PROJECT_NOT_FOUND));

        return projectMapper.projectToDto(project);
    }

    @Override
    public List<ProjectDto> getProjectByName(String projectName) {

        List<Project> projects = projectRepository.findProjectByProjectName(projectName);

        return projectMapper.projectsToDtoList(projects);
    }

    @Override
    public ProjectDto getProjectByProjectIdentifier(String projectIdentifier) throws NotFoundException {

        Project project = projectRepository.findProjectByProjectIdentifier(projectIdentifier)
                .orElseThrow(() -> new NotFoundException(PROJECT_NOT_FOUND));

        return projectMapper.projectToDto(project);
    }

    @Override
    public ProjectDto createProject(ProjectDto projectDto) {

        Project project = projectRepository.save(projectMapper.dtoToProject(projectDto));

        return projectMapper.projectToDto(project);
    }

    @Override
    public ProjectDto updateProject(ProjectDto projectDto) {

        Project project = projectRepository.saveAndFlush(projectMapper.dtoToProject(projectDto));

        return projectMapper.projectToDto(project);
    }

    @Override
    public void deleteProject(Long projectId) {

        projectRepository.deleteById(projectId);

    }


}
