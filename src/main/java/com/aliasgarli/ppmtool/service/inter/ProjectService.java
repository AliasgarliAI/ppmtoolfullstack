package com.aliasgarli.ppmtool.service.inter;

import com.aliasgarli.ppmtool.dto.ProjectDto;
import com.aliasgarli.ppmtool.entity.Project;
import com.aliasgarli.ppmtool.exception.NotFoundException;

import java.util.List;

public interface ProjectService {

    List<ProjectDto> getAllProjects();
    ProjectDto getProjectById(Long projectId) throws NotFoundException;
    List<ProjectDto> getProjectByName(String projectName);
    ProjectDto getProjectByProjectIdentifier(String projectIdentifier) throws NotFoundException;

    ProjectDto createProject(ProjectDto projectDto);

    ProjectDto updateProject(ProjectDto projectDto);

    void deleteProject(Long projectId);

}
